from socket import socket, AF_INET, SOCK_DGRAM
from select import select
from struct import pack, unpack
from Classes.keyboard import Keyboard
from Classes.tunel import Tunel
from Classes.master import Master
import optparse


online=['192.168.0.7','172.16.187.87']
types={0x01:"SUBS",0x02:"ONLINE USERS LIST",0x03:"GET ONLINE USERS LIST"}



def findMaster():
    master = input("insert master IP: ")
    print(type(master))
    if master == '':
        initAsMaster()

def initAsMaster():
    master = Master(ip)
    master.run()

def main():

    findMaster()
    serviceLoop()

def serviceLoop():

    sock = Tunel(ip)
    online.append(ip)
    print(type(online))
    keyboard = Keyboard(online)
    try:
        while True:
            readers = select([keyboard, sock],[] ,[] )[0]

            for reader in readers:
                reader.on_read()
    except KeyboardInterrupt:
        sayGoodBye()

def sayGoodBye():
    print('bye')

def get_ip():

    s = socket(AF_INET, SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    finally:
        s.close()
    return IP

def dbg(msg):
    if options.debug == True:
        print(msg)

if __name__=='__main__':
    
    global ip, outSock, inSock

    parser = optparse.OptionParser()
    parser.add_option ('-d', '--debug', action='store_true',default=False, help='debug', dest="debug")
    (options, args) = parser.parse_args()

    ip = get_ip()
    print("your ip: ", ip)

    main()