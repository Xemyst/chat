import sys
from socket import socket, AF_INET, SOCK_DGRAM

class Keyboard(): #objecte que correspondra al teclat

    def __init__(self,online):
        self.online = online
        self.reader = sys.stdin
        self.sock = socket(AF_INET, SOCK_DGRAM)

    def fileno(self):
        return self.reader.fileno()
        
    def on_read(self):
        txt = self.reader.readline()
        if txt[0] == '-':

            if txt[1:-1] == 'users':
                print ("online users: ", self.online)
            if txt[1:-1] == 'quit':
                sys.exit(0)
            if txt[1:4] == 'doc':
                doc = txt.split()[1]
                sendDoc(doc)
        else:

            txt = txt.encode('utf-8')
            for user in self.online:
                self.sock.sendto(txt,(user,6000))

def sendDoc(doc):
    print(doc)